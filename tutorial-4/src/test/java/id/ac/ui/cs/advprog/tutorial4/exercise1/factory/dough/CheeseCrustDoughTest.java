package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CheeseCrustDoughTest {
    private CheeseCrustDough cheeseCrustDough;

    @Before
    public void setUp() {
        cheeseCrustDough = new CheeseCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Cheese Crust Dough", cheeseCrustDough.toString());
    }
}
