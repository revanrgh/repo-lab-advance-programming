package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class OysterClamsTest {
    private OysterClams oysterClams;

    @Before
    public void setUp() {
        oysterClams = new OysterClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Oyster clams from Long Island Sound", oysterClams.toString());
    }
}
