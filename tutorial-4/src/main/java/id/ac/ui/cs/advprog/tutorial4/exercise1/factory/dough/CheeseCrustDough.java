package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CheeseCrustDough implements Dough {
    public String toString() {
        return "Cheese Crust Dough";
    }
}
