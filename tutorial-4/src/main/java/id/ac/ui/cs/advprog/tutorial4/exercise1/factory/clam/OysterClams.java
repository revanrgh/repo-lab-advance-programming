package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class OysterClams implements Clams {

    public String toString() {
        return "Oyster clams from Long Island Sound";
    }
}
