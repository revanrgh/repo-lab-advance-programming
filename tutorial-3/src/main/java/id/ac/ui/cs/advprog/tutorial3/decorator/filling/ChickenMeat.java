package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        String description = "adding chicken meat";
        return food.getDescription() + ", " + description; 
    }

    @Override
    public double cost() {
        //TODO Implement
        double cost = 4.5;
        return food.cost() + cost;
    }
}
