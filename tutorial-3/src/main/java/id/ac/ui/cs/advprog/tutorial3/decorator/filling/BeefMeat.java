package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends Food {
    Food food;

    public BeefMeat(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        String description = "adding beef meat";
        return food.getDescription() + ", " + description;
    }

    @Override
    public double cost() {
        //TODO Implement
        double cost = 6.0;
        return food.cost() + cost;
    }
}
