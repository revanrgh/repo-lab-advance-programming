package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Food {
    Food food;

    public Lettuce(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        String description = "adding lettuce";
        return food.getDescription() + ", " + description; 
    }

    @Override
    public double cost() {
        //TODO Implement
        double cost = 0.75;
        return food.cost() + cost;
    }
}

