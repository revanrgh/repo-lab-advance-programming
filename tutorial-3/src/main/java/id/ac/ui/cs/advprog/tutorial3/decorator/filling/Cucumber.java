package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        String description = "adding cucumber";
        return food.getDescription() + ", " + description; 
    }

    @Override
    public double cost() {
        //TODO Implement
        double cost = 0.4;
        return food.cost() + cost;
    }
}
