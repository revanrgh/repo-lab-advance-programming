package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        String description = "adding chili sauce";
        return food.getDescription() + ", " + description; 
    }

    @Override
    public double cost() {
        //TODO Implement
        double cost = 0.3;
        return food.cost() + cost;
    }
}

